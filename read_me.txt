Olivier Dumoulin.

Lien GitLab : https://gitlab.com/Surya/shooterM1

Test� sur :
	Wiko Getaway
	Samsung A3 2017

Le shooter est construit pour �tre infini avec une difficult�e croissante suivant l'avancement du score.

Tous les 50 points une pillule de vie est lanc�e.
Tous les 200 points un nouvelle ennemi peut appara�tre, pour compenser, la limite d'ast�roid diminue de m�me.
Tous les 500 points le joueur entre dans une phase de boss, pour chaque phase de boss le nombre de boss augmente.

Ici la difficult� est lin�aire mais une approche logarithmique serait plus adapt�e.

Le background d�file de la m�me fa�on sur tous les �crans de jeu. 2 sprite passe l'un apr�s l'autre puis remonte une fois hors cam�ra.
Dans le cas de tr�s grandes r�solutions la s�paration peut devenir notable.
En cas de ralentissements tr�s important il est �galement possible que des d�calages apparaissent entre les deux objets.

La totalit� des tirs est bas�e sur le syst�me de particule et leur gestion de physique.
Pour �viter de surcharger les performances de la physique, des layers ont �t� sp�cifi�s pour cat�goriser les objets concern�s par les collisions.
L'ensemble des pattern des ennemis correspondent � des modifications d'�mission de particules.

Les contr�les de tir en tactile correspondent simplement au fait de toucher l'�cran plut�t que de rajouter un bouton qui prendrait de l'espace sur l'�cran.
Pour les d�placements tactiles, la v�locit� du ship est augment�e pour le d�placer vers la zone touch�e. 
Plut�t que de faire correspondre un point pr�cis, le ship dispose d'une zone o� doit se situer le point touch�.
Bien qu'approximatif, le placement de la partie du ship sous le doigt correspond � la hitbox, permettant de faire des esquives instinctivement.

Chaque �cran de jeu dispose de sa propre musique. La gestion du son est bas�e sur un objet qui n'est pas d�truit � chaque changement de sc�ne.

Le meilleur score est sauvegard� dans les Player Preferences et est actualis� en cas de reccord battu.





