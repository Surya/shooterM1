﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBackground : MonoBehaviour {

    private Vector3 leftBottom;
    private Vector3 rightTop;
    Vector2 size;
    private float speed;

    // Use this for initialization
    void Start () {
        speed = 2.0F;
        size = GetComponent<SpriteRenderer>().bounds.size;
        leftBottom = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
    }
	
	// Update is called once per frame
	void Update () {
        
        //Vitesse constante du background
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -speed);
        if (transform.position.y + size.y/2 < leftBottom.y)
        {
            //Reposition en avant une fois le background en dessous de la caméra
            Vector3 tmpPos = new Vector3(0, size.y, 10);
            transform.position = tmpPos;
        }
    }
}
