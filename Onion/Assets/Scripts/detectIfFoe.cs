﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detectIfFoe : MonoBehaviour {

    private Vector3 rightTop;
    private Vector3 leftTop;
    private Vector3 leftBottom;
    private Vector3 rightBottom;
    private int currentScore;

    // Use this for initialization
    void Start()
    {
        leftTop = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        rightTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        leftBottom = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightBottom = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
    }
	
	// Update is called once per frame
	void Update () {

        //Récupération du score
        currentScore = gameState.instance.getScorePlayer();

        //Calcul du nombre max d'ennemis en fonction du score
        int maxFoes = 1 + currentScore/200;

        //Nombre d'ennemis en jeu
        int numberFoes = gameState.instance.getNumberFoes(); 


        //Si nombre d'ennemis inférieur au max
        if(numberFoes < maxFoes)
        {

            //Probabilité de génération
            int prob = Random.Range(1, 100);

            //Instanciation uniquement hors phase de boss et avec 50% de chance seulement
            if (prob < 51 && !gameState.instance.getBossMode())
            {
                //Calcul de la position de l'ennemi (quelque soit le type)
                Vector3 tmpPos = new Vector3(0,
                        Random.Range(rightTop.y + 3, 2 * rightTop.y + 3),
                        5);

                //5% de chance pour l'ennemi de type bruit
                if (prob <= 5)
                {
                    GameObject gY = Instantiate(Resources.Load("noiseFoe"), tmpPos, Quaternion.identity) as GameObject;
                    gameState.instance.addFoes(1);
                }
                //5% de chance pour l'ennemi de type vagues
                else if (prob <= 10 && prob > 5)
                {
                    GameObject gY = Instantiate(Resources.Load("waveFoe"), tmpPos, Quaternion.identity) as GameObject;
                    gameState.instance.addFoes(1);
                }
                //15% de chance pour l'ennemi de type rotatif
                else if (prob <= 25 && prob > 10)
                {
                    GameObject gY = Instantiate(Resources.Load("rotatingFoe"), tmpPos, Quaternion.identity) as GameObject;
                    gameState.instance.addFoes(1);
                }
                //25% de chance pour l'ennemi basique
                else if (prob > 25 && prob <= 50)
                {
                    GameObject gY = Instantiate(Resources.Load("baseFoe"), tmpPos, Quaternion.identity) as GameObject;
                    gameState.instance.addFoes(1);
                }                
            }
        }
    }
}
