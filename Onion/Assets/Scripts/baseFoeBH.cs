﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class baseFoeBH : MonoBehaviour {

    private Vector3 leftBottom;
    private Vector3 rightTop;
    private Vector2 size;
    private Vector2 speed;
    private int life;
    private int direction;

    // Use this for initialization
    void Start () {
        size = GetComponent<SpriteRenderer>().bounds.size;
        leftBottom = Camera.main.ViewportToWorldPoint(new Vector3(0, 0.75F, 0));
        rightTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        speed = new Vector2(0, -6.0F);
        life = 2;
        direction = 1;
    }
	
	// Update is called once per frame
	void Update () {

        gameObject.tag = "Foe";

        //Une fois la hauteur atteinte, la descente est stoppée
        if (transform.position.y - size.y/2 <= leftBottom.y) {
            speed.y = 0;            
        }

        //Changement de sens gauche-droite
        if (transform.position.x - size.x / 2 <= leftBottom.x)
        {
            direction = 1;
        }
        if (transform.position.x + size.x / 2 >= rightTop.x)
        {
            direction = -1;
        }

        speed.x = direction * 2.0F;
        
        //Affectation vitesse
        gameObject.GetComponent<Rigidbody2D>().velocity = speed;

    }

    private void OnParticleCollision(GameObject other)
    {
        //Vie retirée en cas de tire percuté
        life--;
        if(life == 0)
        {
            //Ajout des points et destruction une fois la vie à 0
            gameState.instance.addScorePlayer(5);
            gameState.instance.removeFoes(1);
            gameObject.AddComponent<fadeOut>();
        }
    }
}
