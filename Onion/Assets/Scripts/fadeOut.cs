﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fadeOut : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //Diminution de la coueur jusqu'à disparition
        Color cl = GetComponent<SpriteRenderer>().color;
        cl.a -= 0.4F;
        GetComponent<SpriteRenderer>().color = cl;
        if(cl.a < 0)
        {
            //Destruction de l'objet une fois invisible
            Destroy(gameObject);
        }
	}
}
