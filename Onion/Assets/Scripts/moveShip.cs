﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class moveShip : MonoBehaviour {

    public Vector2 speed;
    private Vector3 leftBottom;
    private Vector3 leftTop;
    private Vector3 rightBottom;
    private Vector3 rightTop;

    // Use this for initialization
    void Start () {
        speed = new Vector2(10.0F, 10.0F);
        leftBottom = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 0.66F, 0));

    }
	
	// Update is called once per frame
	void Update () {

        Vector2 size = GetComponent<SpriteRenderer>().bounds.size;
        Vector2 pos = transform.position;

        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(
            Input.GetAxis("Horizontal")*speed.x, 
            Input.GetAxis("Vertical")*speed.y);

        if(Input.touchCount > 0)
        {
            Touch inp = Input.GetTouch(0);
            //Transformation des coordonnées de l'évennement sur l''écran vers les coordonnées du monde
            Vector3 touchPos = Camera.main.ScreenToWorldPoint(inp.position);
            //Changement des vecteurs de vitesse vers la position du doigt
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(
                ((touchPos.x > pos.x-size.x/4 ? 1 : 0)-(touchPos.x < pos.x+size.x/4 ? 1 : 0)) * speed.x, //Convertion booléen vers nombre non native
                ((touchPos.y > pos.y-size.y/4 ? 1 : 0)-(touchPos.y < pos.y+size.y/4 ? 1 : 0)) * speed.y);
        }


        //Blocage du vaisseau dans sa zone définie
        if(pos.y-size.y/2 < leftBottom.y)
        {
            pos.y = leftBottom.y + size.y/2;
        }
        if (pos.x-size.x/2 < leftBottom.x)
        {
            pos.x = leftBottom.x + size.x/2;
        }
        if (pos.y+size.y/2 > rightTop.y)
        {
            pos.y = rightTop.y - size.y/2;
        }
        if (pos.x+size.x/2 > rightTop.x)
        {
            pos.x = rightTop.x - size.x/2;
        }

        transform.position = pos;

        //Mort du vaisseau et changement vers l'écran des scores une fois la vie à 0
        if (gameState.instance.getLifePlayer() < 1)
        {
            SceneManager.LoadScene("endGame");
            soundState.instance.playEnd();
            Destroy(gameObject);

        }

    }

    private void OnParticleCollision(GameObject other)
    {
        //Vie retirée en cas de collision avec un tire ennemi
        gameState.instance.removeLifePlayer(1);
    }

    private void FixedUpdate()
    {
        
    }
}
