﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBoss : MonoBehaviour {
    private Vector3 leftBottom;
    private Vector3 rightTop;
    private Vector2 size;
    private Vector2 speed;
    private int life;
    private bool death;

    // Use this for initialization
    void Start()
    {
        size = GetComponent<BoxCollider2D>().bounds.size;
        leftBottom = Camera.main.ViewportToWorldPoint(new Vector3(0, 0.75F, 0));
        rightTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        speed = new Vector2(0, -2.0F);
        life = 50;
        //Status de mort, entraine le boss à se retirer par le haut de l'écran
        death = false;
    }
	
	// Update is called once per frame
	void Update () {
        gameObject.tag = "Boss";

        //Arrêt de la descente une fois à la bonne hauteur
        if (transform.position.y - size.y / 2 <= leftBottom.y)
        {
            speed.y = 0;
        }


        //En cas de mort du boss
        if (death)
        {
            speed.y = 6.0F;
            //Une fois le boss sorti de l'écran, suppresion de l'objet
            if(transform.position.y - size.y > rightTop.y)
            {
                Destroy(gameObject);
            }
        }

        gameObject.GetComponent<Rigidbody2D>().velocity = speed;
    }

    private void OnParticleCollision(GameObject other)
    {
        life--;
        if (life == 0)
        {
            gameState.instance.addScorePlayer(100);
            death = true;

            //Fin de la phase de boss seulement si le dernier est vaincu
            if (GameObject.FindGameObjectsWithTag("Boss").Length == 1)
            {
                gameState.instance.setBossMode(false);
            }
        }
    }
}
