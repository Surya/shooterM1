﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotatingFoeBH : MonoBehaviour {

    private Vector3 leftBottom;
    private Vector3 rightTop;
    private Vector2 size;
    private Vector2 speed;
    private int life;
    private float posx;

    // Use this for initialization
    void Start()
    {
        size = GetComponent<SpriteRenderer>().bounds.size;
        leftBottom = Camera.main.ViewportToWorldPoint(new Vector3(0, 0.75F, 0));
        rightTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        speed = new Vector2(0, -6.0F);
        life = 3;
        //Position aléatoire sur la coordonnées x
        posx = Random.Range(leftBottom.x + size.x/2, rightTop.x - size.x/2);
    }

    // Update is called once per frame
    void Update()
    {

        gameObject.tag = "Foe";

        //Rotation de l'ennemi entier (sprite + lanceur de particule)
        transform.Rotate(new Vector3(0, 0, 2));

        //Arrêt une fois la descente terminée
        if (transform.position.y - size.y / 2 <= leftBottom.y)
        {
            speed.y = 0;
        }
        //Vers le position x choisis
        if(posx > transform.position.x + size.x / 2)
        {
            speed.x = 2.0F;
        }
        else if(posx < transform.position.x - size.x / 2)
        {
            speed.x = -2.0F;
        }
        else
        {
            speed.x = 0;
        }

        gameObject.GetComponent<Rigidbody2D>().velocity = speed;

    }

    private void OnParticleCollision(GameObject other)
    {
        life--;
        if (life == 0)
        {
            //Attribution du score et destruction une fois la vie à 0
            gameState.instance.addScorePlayer(10);
            gameState.instance.removeFoes(1);
            gameObject.AddComponent<fadeOut>();
        }
    }
}
