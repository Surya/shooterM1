﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detectIfBoss : MonoBehaviour {

    private Vector3 rightTop;
    private Vector3 leftTop;
    private Vector3 leftBottom;
    private Vector3 rightBottom;
    private int currentScore;
    private int nextBoss;

    // Use this for initialization
    void Start()
    {
        leftTop = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        rightTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        leftBottom = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightBottom = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));

        //Pas entre chaque phase de boss
        nextBoss = 500;
    }

    // Update is called once per frame
    void Update () {

        //Récupération du score
        currentScore = gameState.instance.getScorePlayer();

        //Si le pas est atteint en score
        if(currentScore >= nextBoss)
        {

            //Changement en mode phase de boss
            gameState.instance.setBossMode(true);

            //Récupération des ennemis standard présent
            GameObject[] foes = GameObject.FindGameObjectsWithTag("Foe");

            //Intanciation du boss seulement si plus aucun autre type d'ennemi est présent
            if(foes.Length == 0 && GameObject.FindGameObjectsWithTag("Boss").Length==0)
            {
                //Le nombre de boss à faire apparaitre
                int numberBoss = nextBoss / 500;


                //Placement de l'ensemble des boss
                for(int i=1; i < numberBoss+1; ++i)
                {

                    //Placement régulier sur les x pour chaque boss
                    float posx = i * ((rightTop.x - leftTop.x)/(numberBoss+1));
                    

                    Vector3 tmpPos = new Vector3(leftBottom.x+posx,
                        rightTop.y + 3,
                        1);
                    GameObject gY = Instantiate(Resources.Load("Boss"), tmpPos, Quaternion.identity) as GameObject;
                }

                //Affectation du prochain niveau de phase de boss
                nextBoss += 500;
            }
        }
        
	}
}
