﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class noiseFoeBH : MonoBehaviour {

    private Vector3 leftBottom;
    private Vector3 rightTop;
    private Vector2 size;
    private Vector2 speed;
    private int life;
    private float posx;

    // Use this for initialization
    void Start()
    {
        size = GetComponent<SpriteRenderer>().bounds.size;
        leftBottom = Camera.main.ViewportToWorldPoint(new Vector3(0, 0.75F, 0));
        rightTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        speed = new Vector2(0, -6.0F);
        life = 5;
        //Sélection d'une position de placement aléatoire en x
        posx = Random.Range(leftBottom.x + size.x / 2, rightTop.x - size.x / 2);
    }

    // Update is called once per frame
    void Update()
    {
        //Attribution du tag
        gameObject.tag = "Foe";

        //Fin de la descente une fois la hauteur atteinte
        if (transform.position.y - size.y / 2 <= leftBottom.y)
        {
            speed.y = 0;
        }

        //Positionnement vers la coordonnée x désignée
        if (posx > transform.position.x + size.x / 2)
        {
            speed.x = 2.0F;
        }
        else if (posx < transform.position.x - size.x / 2)
        {
            speed.x = -2.0F;
        }
        else
        {
            speed.x = 0;
        }

        gameObject.GetComponent<Rigidbody2D>().velocity = speed;

    }

    private void OnParticleCollision(GameObject other)
    {
        life--;
        if (life == 0)
        {
            //Attribution des points et destruction une fois la vie à 0
            gameState.instance.addScorePlayer(30);
            gameState.instance.removeFoes(1);
            gameObject.AddComponent<fadeOut>();
        }
    }
}
