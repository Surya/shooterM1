﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waveFoeBH : MonoBehaviour {

    private Vector3 leftBottom;
    private Vector3 rightTop;
    private Vector2 size;
    private Vector2 speed;
    private int life;
    private float posx;

    // Use this for initialization
    void Start()
    {
        size = GetComponent<SpriteRenderer>().bounds.size;
        leftBottom = Camera.main.ViewportToWorldPoint(new Vector3(0, 0.75F, 0));
        rightTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        speed = new Vector2(0, -6.0F);
        life = 5;
        //Position aléatoire sur la coordonnée x
        posx = Random.Range(leftBottom.x + size.x / 2, rightTop.x - size.x / 2);
    }

    // Update is called once per frame
    void Update()
    {
        //Attributiondu tag "Foe"
        gameObject.tag = "Foe";
        //Arrêt de la descentre une fois à la bonne hauteur
        if (transform.position.y - size.y / 2 <= leftBottom.y)
        {
            speed.y = 0;
        }
        //Positionnement gauche ou droite vers la coordonnée x choisie
        if (posx > transform.position.x + size.x / 2)
        {
            speed.x = 2.0F;
        }
        else if (posx < transform.position.x - size.x / 2)
        {
            speed.x = -2.0F;
        }
        else
        {
            speed.x = 0;
        }

        gameObject.GetComponent<Rigidbody2D>().velocity = speed;

    }

    private void OnParticleCollision(GameObject other)
    {
        life--;
        if (life == 0)
        {
            //Attribution du score et destruction en cas de vie à 0
            gameState.instance.addScorePlayer(20);
            gameState.instance.removeFoes(1);
            gameObject.AddComponent<fadeOut>();
        }
    }
}
