﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameState : MonoBehaviour {

    public static gameState instance = null;
    //Score du joueur
    private int scorePlayer;
    //Vie du joueur
    private int lifePlayer;
    //Nombre d'ennemis en jeu
    private int numberFoes;
    //Dernier moment où un tire a touché le joueur
    private float lastHit;
    //Booléen de phase de boss
    private bool bossMode;

    // Use this for initialization
    void Start () {
        scorePlayer = 0;
        lifePlayer = 3;
        numberFoes = 0;
        lastHit = 0;
        bossMode = false;
    }
	              
    void Awake() {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }


    //Update is called every frame.
    void Update()
    {
        //Actualisation du score et de la vie pour l'affichage en jeu
        GameObject.FindWithTag("scoreLabel").GetComponent<Text>().text = "" + scorePlayer;
        GameObject.FindWithTag("lifeAmount").GetComponent<Text>().text = "" + lifePlayer;
    }

    public void addScorePlayer(int toAdd)
    {
        scorePlayer += toAdd;
    }
    public int getScorePlayer()
    {
        return scorePlayer;
    }

    public void addLifePlayer(int i)
    {
        lifePlayer += i;
    }

    public void removeLifePlayer(int i)
    {
        //Frame d'une seconde d'invincibilité une fois le joueur touché par quelque chose
        float delta = Time.time - lastHit;
        if(delta > 1.0F)
        {
            lifePlayer -= i;
            lastHit = Time.time;
        }
    }

    public int getLifePlayer()
    {
        return lifePlayer;
    }

    public int getNumberFoes()
    {
        return numberFoes;
    }

    public void addFoes(int i)
    {
        numberFoes += i;
    }

    public void removeFoes(int i)
    {
        if(numberFoes > 0)
        {
            numberFoes -= i;
        }
    }

    public bool getBossMode()
    {
        return bossMode;
    }

    public void setBossMode(bool b)
    {
        bossMode = b;
    }
}
