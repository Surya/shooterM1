﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detectIfBonus : MonoBehaviour {
    

    private Vector2 size;
    private Vector3 rightTop;
    private Vector3 leftTop;
    private static int nextLife;

    // Use this for initialization
    void Start()
    {
        leftTop = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        rightTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        //Pas entre chaque vie donnée
        nextLife = 50;
    }

    // Update is called once per frame
    void Update()
    {

        if (gameState.instance.getScorePlayer() > nextLife)
        {
            // Position de la nouvelle vie
            Vector3 tmpPos = new Vector3(Random.Range(leftTop.x + (size.x / 2), (rightTop.x - (size.x / 2))),
                Random.Range(rightTop.y + (size.y / 2), 2 * rightTop.y + (size.y / 2)),
                8);
            // Instanciation
            GameObject gY = Instantiate(Resources.Load("bonusLife"), tmpPos, Quaternion.identity) as GameObject;

            //Prochain point d'ajout de vie
            nextLife += 50;
        }
        
    }
}
