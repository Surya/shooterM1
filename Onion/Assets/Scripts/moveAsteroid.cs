﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveAsteroid : MonoBehaviour {

    private Vector3 leftBottom;
    private Vector3 rightTop;
    Vector2 size;
    Vector2 speed;

    // Use this for initialization
    void Start () {
        size = GetComponent<SpriteRenderer>().bounds.size;
        leftBottom = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        //Vitesse aléatoire de l'astéroid
        speed = new Vector2(0, -Random.Range(4.0F, 8.0F));
    }
	
	// Update is called once per frame
	void Update () {
        //Affectation de la vitesse
        gameObject.GetComponent<Rigidbody2D>().velocity = speed;
        //Affectation du tag "asteroid"
        gameObject.tag = "asteroid";
        if(transform.position.y+size.y < leftBottom.y)
        {
            //Destruction de l'objet si sorti de la caméra
            Destroy(gameObject);
        }

    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        //Application de dégats si contact avec le joueur
        if(collider.name == "Ship")
        {
            gameState.instance.removeLifePlayer(1);
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        //En cas de tire reçu, destruction de l'objet et augmentation du score
        gameState.instance.addScorePlayer(1);
        gameObject.AddComponent<fadeOut>();
    }

    private void OnDestroy()
    {
        
    }
}
