﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootAgain : MonoBehaviour {

    ParticleSystem shots;


    // Use this for initialization
    void Start () {
        //Récupération du système de particule
        shots = GetComponent<ParticleSystem>();
        var em = shots.emission;
        //Désaction de l'émission de base
        em.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        var em = shots.emission;
        //Si barre espace pressée ou bien écran touché
        bool sp = Input.GetKey(KeyCode.Space) || Input.touchCount > 0;
        if (sp)
        {
            //Activation de l'émission
            em.enabled = true;
            //soundState.instance.touchButtonSound();
        }
        else
        {
            //Désactivation de l'émission
            em.enabled = false;
        }
    }


}
