﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoreManager : MonoBehaviour {

	// Use this for initialization
	void Start () {

        //Récupération du score fait
        int yours = gameState.instance.getScorePlayer();
        //Récupération du meilleur score dans les player preferences
        int best = PlayerPrefs.GetInt("bestScore", 0);
        //Comparaison score fait et meilleur
        if(yours > best)
        {
            //Modification du meilleur score
            PlayerPrefs.SetInt("bestScore", yours);
            best = yours;
        }

        //Actualisation de l'affichage en jeu des score
        GameObject.FindWithTag("yourScore").GetComponent<Text>().text = "" + yours;
        GameObject.FindWithTag("bestScore").GetComponent<Text>().text = "" + best;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
