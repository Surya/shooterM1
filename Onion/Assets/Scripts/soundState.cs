﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundState : MonoBehaviour {
    public static soundState instance = null;
    public AudioClip title;
    public AudioClip game;
    public AudioClip end;
    private AudioSource audioS;


    // Use this for initialization
    void Start () {
        DontDestroyOnLoad(this.gameObject);
        audioS = GetComponent<AudioSource>();
	}

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    // Update is called once per frame
    void Update () {
		
	}

    /// Play a sound
    private void MakeSound(AudioClip originalClip)
    {
        AudioSource.PlayClipAtPoint(originalClip, transform.position);
    }

    //Musique de l'écran titre
    public void playTitle()
    {
        audioS.clip = title;
        audioS.Play();
    }
    //Musique du jeu
    public void playGame()
    {
        audioS.clip = game;
        audioS.Play();
    }
    //Musique de l'écran des scores
    public void playEnd()
    {
        audioS.clip = end;
        audioS.Play();
    }
}
