﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePill : MonoBehaviour {
    private Vector3 leftBottom;
    private Vector3 rightTop;
    Vector2 size;
    Vector2 speed;

    // Use this for initialization
    void Start()
    {
        size = GetComponent<SpriteRenderer>().bounds.size;
        leftBottom = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        speed = new Vector2(0, -6.0F);
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = speed;
        if (transform.position.y + size.y < leftBottom.y)
        {
            Destroy(gameObject);
        }

    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.name == "Ship")
        {
            //Redonne de la vie et se détruit immédiatement en cas de contact avec le joueur
            gameState.instance.addLifePlayer(1);
            Destroy(gameObject);
        }
    }
}
