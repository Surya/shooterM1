﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detectIfAsteroid : MonoBehaviour {
    private GameObject[] respawns;
    private Vector2 siz;
    private Vector3 rightTop;
    private Vector3 leftTop;

    // Use this for initialization
    void Start () {
        leftTop = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        rightTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
    }

    // Update is called once per frame
    void Update()
    {
        //Récupération des astéroids présents
        respawns = GameObject.FindGameObjectsWithTag("asteroid");
        //Nombre d'astéroids possible, décroit en fonction du score
        int maxAsteroid = 5 - gameState.instance.getScorePlayer()/200;

        //Pour éviter un nombre d'astéroid négatif
        if(maxAsteroid < 0)
        {
            maxAsteroid = 0;
        }

        //Récupération de la taille des astéroids
        if (respawns.Length > 0)
        {
            siz.x = respawns[0].GetComponent<SpriteRenderer>().bounds.size.x;
            siz.y = respawns[0].GetComponent<SpriteRenderer>().bounds.size.y;
        }

        //Vérification d'un nombre d'astéroid inférieur au max et hors mode de boss
        if (respawns.Length < maxAsteroid && !gameState.instance.getBossMode())
        {
            if (Random.Range(1, 100) == 50 || respawns.Length < maxAsteroid)
            {
                // Create a new asteroid
                Vector3 tmpPos = new Vector3(Random.Range(leftTop.x + (siz.x / 2),(rightTop.x - (siz.x / 2))),
                    Random.Range(rightTop.y + (siz.y / 2),2*rightTop.y + (siz.y/2)),
                    8);
                // Instanciation
                GameObject gY = Instantiate (Resources.Load ("asteroidSP"), tmpPos, Quaternion.identity) as GameObject;
            }
        }
    }
}
